# GameDevThemes
A very simple theme using [Jekyll](http://jekyllrb.com/) on [GitLab Pages](https://pages.gitlab.io/).

## Running locally
1. Fork this repository
2. Clone the fork to your computer: `git clone git@gitlab.com:USERNAME/theme-blog-simple.git`
3. Start local server via `jekyll serve`
4. Access the theme at [http://localhost:4000/theme-blog-simple](http://localhost:4000/theme-blog-simple)

## License
[MIT License](LICENSE.md)
